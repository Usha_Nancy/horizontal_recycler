package com.grootan.horizontalrecycler;

import android.view.View;

public interface ItemClickInterface {

    void onItemClicked(View view,int position,String string);
}
