package com.grootan.horizontalrecycler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.Holder> {

    private Context context;
    private List<String> values;
    private ItemClickInterface itemClickInterface;

    public void setClickListener(ItemClickInterface itemClickListener)
    {
        this.itemClickInterface = itemClickListener;
    }
    public HorizontalAdapter(Context context, List<String> values) {
        this.context = context;
        this.values = values;
    }

    @NonNull
    @Override
    public HorizontalAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v1 = inflater.inflate(R.layout.recycler_items, parent, false);
        HorizontalAdapter.Holder viewHolder = new Holder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HorizontalAdapter.Holder holder, final int position) {
        if (values != null) {
            holder.recyclerItem.setText(values.get(position));

        }
        holder.recyclerItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickInterface.onItemClicked(view, position, values.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextView recyclerItem;

        public Holder(@NonNull View itemView) {
            super(itemView);

            recyclerItem = itemView.findViewById(R.id.recycler_item);

        }
    }
}
