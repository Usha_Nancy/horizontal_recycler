package com.grootan.horizontalrecycler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ItemClickInterface {

    private RecyclerView horizontalRecycler;
    private LinearLayoutManager linearLayoutManager;
    private HorizontalAdapter horizontalAdapter;
    private List<String> values = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        horizontalRecycler = findViewById(R.id.horizontal_recycler);
        // Adding items to RecyclerView.
        AddItemsToRecyclerViewList();

        // calling constructor of adapter
        // with  context and list as parameters
        horizontalAdapter = new HorizontalAdapter(MainActivity.this, values);

        // set clicklistener
        horizontalAdapter.setClickListener(this);
        // Set Horizontal Layout Manager
        // for Recycler view
        linearLayoutManager
                = new LinearLayoutManager(
                MainActivity.this,
                LinearLayoutManager.HORIZONTAL,
                false);
        horizontalRecycler.setLayoutManager(linearLayoutManager);

        // Set adapter on recycler view
        horizontalRecycler.setAdapter(horizontalAdapter);
    }

    private void AddItemsToRecyclerViewList() {

        values.add("test1");
        values.add("test2");
        values.add("test3");
        values.add("test4");
        values.add("test5");
        values.add("test6");
        values.add("test7");
        values.add("test8");
        values.add("test9");
        values.add("test10");
    }

    @Override
    public void onItemClicked(View view, int position, String string) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
    }
}
